For running locally:
1. Create .env file with Salesforce credentials ('USERNAME', 'PASSWORD', 'SECURITY_TOKEN')
2. Use list_of_dicts_to_local_csv_file() to write to local csv
3. Use list_of_dicts_to_csv_string() and csv_string_to_s3() to write to s3